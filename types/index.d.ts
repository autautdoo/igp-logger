export let Logger: typeof import("./lib/logger").Logger;
export let SimpleLogger: typeof import("./lib/loggerImplementations/simpleLogger");
export let SlackLogger: typeof import("./lib/loggerImplementations/slackLogger");
export let WinstonLogger: typeof import("./lib/loggerImplementations/winstonLogger");
export let LOG_LEVEL: {
    fatal: string;
    error: string;
    warn: string;
    info: string;
    debug: string;
};
export let logLevelFromNumber: typeof import("./lib/log_level").logLevelFromNumber;
//# sourceMappingURL=index.d.ts.map