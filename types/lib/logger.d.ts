export class Logger {
    /**
      * Creates a Logger instance. accepts logger backend implementations and uses only
      * SimpleLogger by default
      *
      * @param {string} logLevel - The log level.
      * @param {LoggerI[]} [loggers] - Optional logger implementation, defaults to SimpleLogger.
      * @param {boolean} [useApm] - Whether to use APM if no loggers specified and SimpleLogger is
      * used.
      */
    constructor(logLevel: string, loggers?: LoggerI[], useApm?: boolean);
    logLevel: string;
    loggers: LoggerI[];
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    log(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    info(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    warn(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    error(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    debug(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    fatal(message: string, object?: any): void;
    /**
    * Logs the request error.
    * @param {string} reqMethod - The request method.
    * @param {string} reqUrl - The request URL.
    * @param {Object} reqBody - The request body.
    * @param {Object} reqHeaders - The request headers.
    * @param {number} resStatusCode - The response status code.
    * @param {Object} resBody - The response body.
    * @description Logs the request error.
    */
    logRequestError(reqMethod: string, reqUrl: string, reqBody: any, reqHeaders: any, resStatusCode: number, resBody: any): void;
}
/**
 * Interface for an logging backend implementation.
 * @interface LoggerI
 */
declare class LoggerI {
    /**
      * Logs an message with set logLevel.
      * @param {string} message - The message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    log(message: string, object?: any): void;
    /**
      * Logs a info message.
      * @param {string} message - The warning message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    info(message: string, object?: any): void;
    /**
      * Logs a warning message.
      * @param {string} message - The warning message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    warn(message: string, object?: any): void;
    /**
      * Logs an error message.
      * @param {string} message - The error message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    error(message: string, object?: any): void;
    /**
      * Logs an debug message.
      * @param {string} message - The error message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    debug(message: string, object?: any): void;
    /**
      * Logs an fatal message.
      * @param {string} message - The error message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    fatal(message: string, object?: any): void;
    /**
     * Logs the request error.
     */
    logRequestError(reqMethod: any, reqUrl: any, reqBody: any, reqHeaders: any, resStatusCode: any, resBody: any): void;
}
export {};
//# sourceMappingURL=logger.d.ts.map