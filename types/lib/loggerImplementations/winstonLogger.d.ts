export = WinstonLogger;
/**
 * @class WinstonLogger
 * @implements {LoggerI}
 */
declare class WinstonLogger implements LoggerI {
    /**
    * @param {LOG_LEVELS} logLevel - The log level to use
    * @param {string} appName - App name to include in the logs
    * @param {string} env - Platform/Environment to include in the logs
    * @param {Object} [loggerOptions] - Additional winston logger options
    */
    constructor(logLevel: LOG_LEVELS, appName: string, env: string, loggerOptions?: any);
    appName: string;
    env: string;
    logLevel: LOG_LEVELS;
    loggerInstance: winston.Logger;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    log(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    info(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    warn(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    error(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    debug(message: string, object?: any): void;
    /**
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
    fatal(message: string, object?: any): void;
    /**
    * Logs the request error.
    * @param {string} reqMethod - The request method.
    * @param {string} reqUrl - The request URL.
    * @param {Object} reqBody - The request body.
    * @param {Object} reqHeaders - The request headers.
    * @param {number} resStatusCode - The response status code.
    * @param {Object} resBody - The response body.
    * @returns {void}
    * @description Logs the request error.
    */
    logRequestError(reqMethod: string, reqUrl: string, reqBody: any, reqHeaders: any, resStatusCode: number, resBody: any): void;
}
import winston = require("winston");
//# sourceMappingURL=winstonLogger.d.ts.map