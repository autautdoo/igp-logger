export = SimpleLogger;
/**
 * @class SimpleLogger
 * @implements {LoggerI}
 */
declare class SimpleLogger implements LoggerI {
    /**
     * @param {LOG_LEVEL} logLevel - The log level to use
     * @param {boolean} useApm - Whether to use APM
     */
    constructor(logLevel: LOG_LEVEL, useApm: boolean);
    logLevel: LOG_LEVEL;
    useApm: boolean;
    loggerInstance: this;
    apmCaptureError: any;
    log(message: any, object: any): void;
    info(message: any, object: any): void;
    warn(message: any, object: any): void;
    error(message: any, object: any): void;
    debug(message: any, object: any): void;
    fatal(message: any, object: any): void;
    noApmError(message: any, object: any): void;
    logMessage(message: any, level: any, object: any): void;
    logErrorMessage(sendToApm: any, message: any, level: any, object: any): void;
    logRequestError(reqMethod: any, reqUrl: any, reqBody: any, reqHeaders: any, resStatusCode: any, resBody: any): void;
    isLogLevelAllowed(level: any): boolean;
    extendConsoleLogWithTrace(): void;
}
//# sourceMappingURL=simpleLogger.d.ts.map