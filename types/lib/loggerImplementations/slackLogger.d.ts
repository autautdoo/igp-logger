export = SlackLogger;
/**
 * @class SimpleLogger
 * @implements {LoggerI}
 */
declare class SlackLogger implements LoggerI {
    /**
     * @param  {String} webhookUrl - The slack incoming webhook URL
     * @param {LOG_LEVELS} logLevel - The log level to use
     */
    constructor(webhookUrl: string, logLevel: LOG_LEVELS);
    webhookUrl: string;
    logLevel: LOG_LEVELS;
    /**
     * @param {String} level - Level string to check
     */
    isLogLevelAllowed(level: string): boolean;
    /**
      * Logs an message with set logLevel.
      * @param {string} message - The message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    log(message: string, object?: any): void;
    /**
      * Logs a info message.
      * @param {string} message - The warning message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    info(message: string, object?: any): void;
    /**
      * Logs a warning message.
      * @param {string} message - The warning message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    warn(message: string, object?: any): void;
    /**
      * Logs an error message.
      * @param {string} message - The error message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    error(message: string, object?: any): void;
    /**
      * Logs an debug message.
      * @param {string} message - The error message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    debug(message: string, object?: any): void;
    /**
      * Logs an fatal message.
      * @param {string} message - The error message to log.
      * @param {Object} [object] - Optional additional object to log.
      */
    fatal(message: string, object?: any): void;
    /**
     * Logs the request error.
     */
    logRequestError(reqMethod: any, reqUrl: any, reqBody: any, reqHeaders: any, resStatusCode: any, resBody: any): void;
    /**
    * @param {String} level - The log level
    * @param {String} msg - The message to log
    * @param {any} object - additional object to send to slack
     */
    sendToSlack(level: string, msg: string, object: any): void;
}
//# sourceMappingURL=slackLogger.d.ts.map