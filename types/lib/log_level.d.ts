export namespace COLORS {
    let fatal: string;
    let error: string;
    let warn: string;
    let info: string;
    let debug: string;
}
export const LOG_LEVEL_SEVERITY: {
    [x: string]: number;
};
export type LOG_LEVEL = string;
export namespace LOG_LEVEL {
    let fatal_1: string;
    export { fatal_1 as fatal };
    let error_1: string;
    export { error_1 as error };
    let warn_1: string;
    export { warn_1 as warn };
    let info_1: string;
    export { info_1 as info };
    let debug_1: string;
    export { debug_1 as debug };
}
/**
 * @param {number} logLevel - The log level as a number.
 * @returns {LOG_LEVEL}
 */
export function logLevelFromNumber(logLevel: number): LOG_LEVEL;
//# sourceMappingURL=log_level.d.ts.map