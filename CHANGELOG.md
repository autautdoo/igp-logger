# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.1.0](https://bitbucket.org/autautdoo/igp-logger/compare/v3.0.1...v3.1.0) (2024-08-06)


### Features

* add types ([35465e2](https://bitbucket.org/autautdoo/igp-logger/commit/35465e2ea5d67e01b21d9702a9fcbe971e2bcfaa))

### [3.0.1](https://bitbucket.org/autautdoo/igp-logger/compare/v3.0.0...v3.0.1) (2024-07-16)


### Bug Fixes

* circular dependency issues ([e35362d](https://bitbucket.org/autautdoo/igp-logger/commit/e35362d2d2222e683b1d2aabf4d3827ec84b2661))

## [3.0.0](https://bitbucket.org/autautdoo/igp-logger/compare/v2.1.1...v3.0.0) (2024-07-15)


### ⚠ BREAKING CHANGES

* refactor logger and add ability to use multiple loggers

### Features

* refactor logger and add ability to use multiple loggers ([6574c8e](https://bitbucket.org/autautdoo/igp-logger/commit/6574c8eed39d9e1b53ba006233181c4e9921ed2b))

## [2.1.1](https://bitbucket.org/autautdoo/igp-logger/compare/v2.1.0...v2.1.1) (2021-01-11)

### Bug Fixes

* IGPIMP-2070 - Fix APM error message formatting

## [2.1.0](https://bitbucket.org/autautdoo/igp-logger/compare/v2.0.0...v2.1.0) (2021-01-10)

### Bug Fixes

* IGPIMP-2062  
  Quick fix to remove `colors` package due to the author intentionally breaking the package in https://github.com/Marak/colors.js/commit/074a0f8ed0c31c35d13d28632bd8a049ff136fb6  
  This should have been done regardless - so we're not extending the String prototype

## [2.0.0](https://bitbucket.org/autautdoo/igp-logger/compare/v1.0.6...v2.0.0) (2020-10-25)

* added elastic-apm-agent error capturing
* updated dependencies

## [1.0.6](https://bitbucket.org/autautdoo/igp-logger/compare/v1.0.5...v1.0.6) (2019-04-15)


### Bug Fixes

* added readme.md file ([db7ac26](https://bitbucket.org/autautdoo/igp-logger/commits/db7ac26))



## [1.0.5](https://bitbucket.org/autautdoo/igp-logger/compare/v1.0.3...v1.0.5) (2019-04-15)



Changelog
All notable changes to this project will be documented in this file.

The format is based on Keep a Changelog, and this project adheres to Semantic Versioning.

[1.0.0]
IGP-7403 - Extract internal library "logger" to separate repository
IGP-7402 - Extend current logger with options for logging levels
