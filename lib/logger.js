const SimpleLogger = require('./loggerImplementations/simpleLogger');


/**
 * Interface for an logging backend implementation.
 * @interface LoggerI
 */
// eslint-disable-next-line no-unused-vars
class LoggerI {
  /**
    * Logs an message with set logLevel.
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  log(message, object) { }

  /**
    * Logs a info message.
    * @param {string} message - The warning message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  info(message, object) { }

  /**
    * Logs a warning message.
    * @param {string} message - The warning message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  warn(message, object) { }

  /**
    * Logs an error message.
    * @param {string} message - The error message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  error(message, object) { }

  /**
    * Logs an debug message.
    * @param {string} message - The error message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  debug(message, object) { }

  /**
    * Logs an fatal message.
    * @param {string} message - The error message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  fatal(message, object) { }

  /**
   * Logs the request error.
   */
  logRequestError(
    reqMethod,
    reqUrl,
    reqBody,
    reqHeaders,
    resStatusCode,
    resBody,
  ) { }
}

class Logger {
  /**
    * Creates a Logger instance. accepts logger backend implementations and uses only
    * SimpleLogger by default
    *
    * @param {string} logLevel - The log level.
    * @param {Array<LoggerI>} [loggers] - Optional logger implementation, defaults to SimpleLogger.
    * @param {boolean} [useApm] - Whether to use APM if no loggers specified and SimpleLogger is
    * used.
    */
  constructor(logLevel, loggers, useApm = false) {
    this.logLevel = logLevel;
    this.loggers = loggers || [new SimpleLogger(logLevel, useApm)];
  }


  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  log(message, object) {
    const len = this.loggers.length;
    for (let x = 0; x < len; x++) {
      this.loggers[x].log(message, object);
    }
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  info(message, object) {
    const len = this.loggers.length;
    for (let x = 0; x < len; x++) {
      this.loggers[x].info(message, object);
    }
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  warn(message, object) {
    const len = this.loggers.length;
    for (let x = 0; x < len; x++) {
      this.loggers[x].warn(message, object);
    }
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  error(message, object) {
    const len = this.loggers.length;
    for (let x = 0; x < len; x++) {
      this.loggers[x].error(message, object);
    }
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  debug(message, object) {
    const len = this.loggers.length;
    for (let x = 0; x < len; x++) {
      this.loggers[x].debug(message, object);
    }
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  fatal(message, object) {
    const len = this.loggers.length;
    for (let x = 0; x < len; x++) {
      this.loggers[x].fatal(message, object);
    }
  }

  /**
  * Logs the request error.
  * @param {string} reqMethod - The request method.
  * @param {string} reqUrl - The request URL.
  * @param {Object} reqBody - The request body.
  * @param {Object} reqHeaders - The request headers.
  * @param {number} resStatusCode - The response status code.
  * @param {Object} resBody - The response body.
  * @description Logs the request error.
  */
  logRequestError(
    reqMethod,
    reqUrl,
    reqBody,
    reqHeaders,
    resStatusCode,
    resBody,
  ) {
    const len = this.loggers.length;
    for (let x = 0; x < len; x++) {
      this.loggers[x].logRequestError(
        reqMethod,
        reqUrl,
        reqBody,
        reqHeaders,
        resStatusCode,
        resBody,
      );
    }
  }
}

module.exports = {
  Logger,
};
