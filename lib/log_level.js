
const COLORS = {
  fatal: 'magenta',
  error: 'red',
  warn: 'yellow',
  info: 'green',
  debug: 'grey',
};

/** @enum {string} */
const LOG_LEVEL = {
  fatal: 'fatal',
  error: 'error',
  warn: 'warn',
  info: 'info',
  debug: 'debug',
};

const LOG_LEVEL_SEVERITY = {
  [LOG_LEVEL.fatal]: 0,
  [LOG_LEVEL.error]: 1,
  [LOG_LEVEL.warn]: 2,
  [LOG_LEVEL.info]: 3,
  [LOG_LEVEL.debug]: 4,
};

/**
 * @param {number} logLevel - The log level as a number.
 * @returns {LOG_LEVEL}
 */
function logLevelFromNumber(logLevel) {
  let level;
  for (const [key, val] of Object.entries(LOG_LEVEL_SEVERITY)) {
    if (val === logLevel) {
      level = key;
      break;
    }
  }

  if (level) {
    return level;
  }
  throw new Error(`Invalid log level: ${logLevel}`);
}


module.exports = {
  COLORS,
  LOG_LEVEL_SEVERITY,
  LOG_LEVEL,
  logLevelFromNumber,
};
