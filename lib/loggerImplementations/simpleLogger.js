/* eslint-disable no-console */
const chalk = require('chalk');
const util = require('util');
const {LOG_LEVEL_SEVERITY, COLORS} = require('../log_level');

/**
 * @class SimpleLogger
 * @implements {LoggerI}
 */
class SimpleLogger {
  /**
   * @param {LOG_LEVEL} logLevel - The log level to use
   * @param {boolean} useApm - Whether to use APM
   */
  constructor(logLevel, useApm) {
    this.logLevel = logLevel;
    this.useApm = useApm;
    this.loggerInstance = this;
    try {
      if (this.useApm) {
        const apm = require('elastic-apm-node');

        this.apmCaptureError = apm.captureError.bind(apm);
      } else {
        this.apmCaptureError = () => { };
      }
    } catch (er) {
      // no apm, it's ok
      this.apmCaptureError = () => { };
    }
  }

  log(message, object) {
    this.logMessage(message, 'info', object);
  }

  info(message, object) {
    this.logMessage(message, 'info', object);
  }

  warn(message, object) {
    this.logMessage(message, 'warn', object);
  }

  error(message, object) {
    this.logErrorMessage(true, message, 'error', object);
  }

  debug(message, object) {
    this.logMessage(message, 'debug', object);
  }

  fatal(message, object) {
    this.logErrorMessage(true, message, 'fatal', object);
  }

  noApmError(message, object) {
    this.logErrorMessage(false, message, 'error', object);
  }

  logMessage(message, level, object) {
    if (!this.isLogLevelAllowed(level)) {
      return null;
    }

    if (!object && typeof message === 'object') {
      return console.log(util.inspect(message, false, null));
    }

    if (object) {
      console.log(`${chalk[COLORS[level]](message)} \n`, util.inspect(object, false, null));
    } else {
      console.log(`${chalk[COLORS[level]](message)}`);
    }
  }

  logErrorMessage(sendToApm, message, level, object) {
    if (!this.isLogLevelAllowed(level)) {
      return null;
    }

    // If we only log an object (can be error as well) without a separate log message, just send what we have
    // Object will be stringified to generate the message, and stored structured in the `custom` property
    if (!object && typeof message === 'object') {
      if (sendToApm) {
        this.apmCaptureError(message, {
          custom: message,
        });
      }

      return console.error(util.inspect(message, false, null));
    }

    // If we log an object (can be error as well) with a separate log message
    if (object) {
      if (sendToApm) {
        // First parameter needs to be either an Error object, or a string.
        // If we're throwing another type of object, it's captured regardless in the `custom` property.
        // And we supply the message separately so both the log message and error message is stored in APM.
        this.apmCaptureError(object instanceof Error ? object : message, {
          custom: object,
          message,
        });
      }

      return console.error(`${chalk[COLORS[level]](message)} \n`, util.inspect(object, false, null));
    }

    // If we just log a string message without an attached object/error
    if (sendToApm) {
      this.apmCaptureError(message);
    }

    console.error(`${chalk[COLORS[level]](message)}`);
  }

  logRequestError(
    reqMethod,
    reqUrl,
    reqBody,
    reqHeaders,
    resStatusCode,
    resBody,
  ) {
    // logger.logRequestError(req.method, res.req.originalUrl, req.body, res.statusCode, res.resPayload);
    console.log(`${chalk.red('Error')} on request ${chalk.blue(reqMethod)} ${chalk.green(reqUrl)}`);
    if (reqBody) {
      console.log(`${chalk.blue('Request body')} ${JSON.stringify(reqBody || {})}`);
    }
    if (reqHeaders) {
      console.log(`${chalk.blue('Request headers')} ${JSON.stringify(reqHeaders || {})}`);
    }
    console.log(`${chalk.blue('Response')} ${chalk.red(resStatusCode)} body:${JSON.stringify(resBody || {})}`);
  }

  isLogLevelAllowed(level) {
    return LOG_LEVEL_SEVERITY[level] <= LOG_LEVEL_SEVERITY[this.logLevel];
  }

  extendConsoleLogWithTrace() {
    ['error'].forEach((methodName) => {
      const originalMethod = console[methodName];
      console[methodName] = (...args) => {
        try {
          throw new Error();
        } catch (error) {
          try {
            originalMethod.apply(
              console,
              [
                ...args,
                `\n${chalk.red('SOURCE:')} `,
                (
                  error
                    .stack // Grabs the stack trace
                    .split('\n')[4] // Grabs third line
                    .trim() // Removes spaces
                    .substring(3) // Removes three first characters ("at ")
                    .replace(__dirname, '') // Removes script folder path
                    .replace(/\s\(./, ' at ') // Removes first parentheses and replaces it with " at "
                    .replace(/\)/, '') // Removes last parentheses
                ),
              ],
            );
            // eslint-disable-next-line no-empty
          } catch (err) { }
        }
      };
    });
  }
}

module.exports = SimpleLogger;
