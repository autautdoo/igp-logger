const LOG_LEVEL_SEVERITY = require('../log_level').LOG_LEVEL_SEVERITY;
const fetch = require('node-fetch');

/**
 * @class SimpleLogger
 * @implements {LoggerI}
 */
class SlackLogger {
  /**
   * @param  {String} webhookUrl - The slack incoming webhook URL
   * @param {LOG_LEVELS} logLevel - The log level to use
   */
  constructor(webhookUrl, logLevel) {
    this.webhookUrl = webhookUrl;
    this.logLevel = logLevel;
  }

  /**
   * @param {String} level - Level string to check
   */
  isLogLevelAllowed(level) {
    return LOG_LEVEL_SEVERITY[level] <= LOG_LEVEL_SEVERITY[this.logLevel];
  }

  /**
    * Logs an message with set logLevel.
    * @param {string} message - The message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  log(message, object) {
    if (this.isLogLevelAllowed('info')) {
      this.sendToSlack('INFO', message, object);
    }
  }


  /**
    * Logs a info message.
    * @param {string} message - The warning message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  info(message, object) {
    if (this.isLogLevelAllowed('info')) {
      this.sendToSlack('INFO', message, object);
    }
  }

  /**
    * Logs a warning message.
    * @param {string} message - The warning message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  warn(message, object) {
    if (this.isLogLevelAllowed('warn')) {
      this.sendToSlack('WARN', message, object);
    }
  }

  /**
    * Logs an error message.
    * @param {string} message - The error message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  error(message, object) {
    if (this.isLogLevelAllowed('error')) {
      this.sendToSlack('ERROR', message, object);
    }
  }

  /**
    * Logs an debug message.
    * @param {string} message - The error message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  debug(message, object) {
    if (this.isLogLevelAllowed('debug')) {
      this.sendToSlack('DEBUG', message, object);
    }
  }

  /**
    * Logs an fatal message.
    * @param {string} message - The error message to log.
    * @param {Object} [object] - Optional additional object to log.
    */
  fatal(message, object) {
    if (this.isLogLevelAllowed('fatal')) {
      this.sendToSlack('FATAL', message, object);
    }
  }

  /**
   * Logs the request error.
   */
  logRequestError(
    reqMethod,
    reqUrl,
    reqBody,
    reqHeaders,
    resStatusCode,
    resBody,
  ) { }

  /**
  * @param {String} level - The log level
  * @param {String} msg - The message to log
  * @param {any} object - additional object to send to slack
   */
  sendToSlack(level, msg, object) {
    try {
      const mes = {
        blocks: [
          {
            type: 'section',
            text: {
              type: 'plain_text',
              text: `[${level}]: ${msg}`,
            },
          },
        ]};
      if (object) {
        const obj = object instanceof Error ?
          JSON.stringify({
            message: object.message,
            stack: object.stack,
            name: object.name,
          }) : JSON.stringify(object);

        mes.blocks.push({
          type: 'rich_text',
          elements: [
            {
              type: 'rich_text_preformatted',
              elements: [
                {
                  type: 'text',
                  text: obj,
                },
              ],
            },
          ],
        });
      }

      fetch(this.webhookUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(mes),
      });
      // eslint-disable-next-line no-empty
    } catch (e) {
    }
  }
}

module.exports = SlackLogger;
