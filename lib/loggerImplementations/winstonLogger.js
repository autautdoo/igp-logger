const winston = require('winston');
const {COLORS, LOG_LEVEL_SEVERITY} = require('../log_level');

/**
 * @class WinstonLogger
 * @implements {LoggerI}
 */
class WinstonLogger {
  /**
  * @param {LOG_LEVEL} logLevel - The log level to use
  * @param {string} appName - App name to include in the logs
  * @param {string} env - Platform/Environment to include in the logs
  * @param {Object} [loggerOptions] - Additional winston logger options
  */
  constructor(logLevel, appName, env, loggerOptions) {
    this.appName = appName;
    this.env = env;
    this.logLevel = logLevel;
    const envFormat = winston.format((info) => {
      const splat = info[Symbol.for('splat')];

      if (splat && splat.length) {
        info.metadata = splat.reduce((acc, curr) => {
          if (curr != null) {
            if (typeof curr === 'object') {
              for (const key in curr) {
                if (Object.hasOwn(info, key)) {
                  delete info[key];
                }
              }
              acc.push(curr);
              return acc;
            } else {
              acc.push(curr);
              return acc;
            }
          }
          return acc;
        }, []);
        if (info.metadata.length === 0) {
          delete info.metadata;
        }
      }

      info.env = this.env;
      info.appName = this.appName;
      return info;
    });

    this.loggerInstance = winston.createLogger({
      levels: LOG_LEVEL_SEVERITY,
      format: winston.format.combine(
        envFormat(),
        winston.format.timestamp(),
        winston.format.json(),
        winston.format.colorize({
          all: true,
          colors: COLORS,
        }),
      ),
      transports: [
        new winston.transports.Console(),
      ],
      level: logLevel,
      ...loggerOptions,
    });
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  log(message, object) {
    this.loggerInstance.log('info', message, object);
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  info(message, object) {
    this.loggerInstance.log('info', message, object);
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  warn(message, object) {
    this.loggerInstance.log('warn', message, object);
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  error(message, object) {
    this.loggerInstance.log('error', message, object);
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  debug(message, object) {
    this.loggerInstance.log('debug', message, object);
  }

  /**
  * @param {string} message - The message to log.
  * @param {Object} [object] - Optional additional object to log.
  */
  fatal(message, object) {
    this.loggerInstance.log('fatal', message, object);
  }

  /**
  * Logs the request error.
  * @param {string} reqMethod - The request method.
  * @param {string} reqUrl - The request URL.
  * @param {Object} reqBody - The request body.
  * @param {Object} reqHeaders - The request headers.
  * @param {number} resStatusCode - The response status code.
  * @param {Object} resBody - The response body.
  * @returns {void}
  * @description Logs the request error.
  */
  logRequestError(
    reqMethod,
    reqUrl,
    reqBody,
    reqHeaders,
    resStatusCode,
    resBody,
  ) {
    this.loggerInstance.error('Error on request', {
      reqMethod,
      reqUrl,
      reqBody,
      reqHeaders,
      resStatusCode,
      resBody,
    });
  }
}

module.exports = WinstonLogger;
