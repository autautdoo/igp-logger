module.exports = {
  Logger: require('./lib/logger').Logger,
  SimpleLogger: require('./lib/loggerImplementations/simpleLogger'),
  SlackLogger: require('./lib/loggerImplementations/slackLogger'),
  WinstonLogger: require('./lib/loggerImplementations/winstonLogger'),
  LOG_LEVEL: require('./lib/log_level').LOG_LEVEL,
  logLevelFromNumber: require('./lib/log_level').logLevelFromNumber,
};
